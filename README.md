# Sciensa | Backend API Test

### Pré-requisitos
  - nodejs
  - npm
  - docker-compose

### Dependências do ambiente de produção
  - body-parser: 1.19.0
  - express: 4.17.1
  - mongoose: 5.7.3

### Dependências do ambiente desenvolvimento
  - chai: 4.2.0
  - chai-http: 4.3.0
  - mocha: 0.2.0
  - nodemon: 1.19.3
  - request: 2.88.0
  - should: 13.2.3
  
### Instruções para executar o projeto
#### Clonar esse repositorio e acessar a pasta raíz do projeto
#### MongoDB
Para esse projeto, eu estou usando um cluster do MongoDB no MongoDB Atlas (https://www.mongodb.com/cloud/atlas)
Para usar o MongoDB do Docker, mudar o conteúdo das linhas 8 a 11 do arquivo src/server.js de:

```
mongoose.connect('mongodb+srv://sciensa:trainees2019@sciensabackend-txr2s.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
```

##### Para:

```
    connection() {
    mongoose.connect(mongodb://mongodb:27017/backendsciensa, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }).catch(error => console.log('Database error: ', error));
  }
```  

connection() {
    mongoose.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    }).catch(error => console.log('Database error: ', error));
  }

#### Instalar dependências com npm install

```
    npm install
```  
#### Buildar as imagens com docker compose

```
    docker-compose build
```  
#### Iniciar o docker compose

```
    docker-compose up -d
```    

### Rotas
#### Gêneros

```
    GET: /genres => Lista os gêneros cadastrados
    GET: /genres/:id => Mostra um gênero específico
    POST: /genres => Cria um gênero novo
    PUT: /genres/:id => Edita um gênero específico
```    

#### Artistas

```
    GET: /artists => Lista os artistas cadastrados
    GET: /artists/:id => Mostra um artista específico
    POST: /artists => Cria um artista novo
    PUT: /artists/:id => Edita um artista específico
```    

#### Filmes

```
    GET: /movies => Lista os filmes cadastrados
    GET: /movies/:id => Mostra um filme específico
    POST: /movies => Cria um filme novo
    PUT: /movies/:id => Edita um filme específico
    DELETE: /movies/:id => Edita um filme específico
```    

# Sciensa: Teste de desenvolvedor back-end

Neste teste, avaliaremos a sua capacidade de implementar corretamente uma API REST.
  - Faça um fork desse repositório
  - Implemente a API documentada no arquivo `api.yml`, presente na raiz deste repositório
  - Use o repositório de dados de sua preferência (mongo, redis, mysql, mssql etc)
  - Forneça acesso de leitura do seu fork para o avaliador da Sciensa

# Diferenciais que levaremos em consideração ao avaliar o seu teste!
  - Cobertura de testes
  - Automação (imagem de docker e ambiente local containerizado com docker-compose)
  - Estrutura do projeto
  - Qualidade de código

Boa sorte!  
#vamosjuntos