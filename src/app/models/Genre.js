const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const GenreSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
    },
});
GenreSchema.set('timestamps', true);
module.exports = mongoose.model('Genre', GenreSchema);