const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const MovieSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    releaseYear: {
        type: Number,
        required: true
    },
    genres: [{
        type :mongoose.Schema.Types.ObjectId,
        ref: 'Genre'
    }],
    director: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist'
    },
    cast: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist'
    }]
});
MovieSchema.set('timestamps', true);
module.exports = mongoose.model('Movie', MovieSchema);