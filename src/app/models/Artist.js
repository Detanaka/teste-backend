const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const ArtistSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    dateOfBirth: {
        type: Date,
        required: true
    }
});
ArtistSchema.set('timestamps', true);
module.exports = mongoose.model('Artist', ArtistSchema);