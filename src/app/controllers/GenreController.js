const Genre = require('../models/Genre');

module.exports = {

    // Resource to be called through the method GET and the path /genres
    async index(req, res){

        let { page } = req.query;
        let { size } = req.query;

        if(!page || (page < 1)){
            page = 1;
        }else{
            page = parseInt(page);
        }

        if(!size || (size < 10)){
            size = 10;
        }else{
            size = parseInt(size);
        }

        let searchDescription = req.query.description;

        if(!searchDescription){
            searchDescription = ""
        }

        let numberOfRecords = 0;
        await Genre.find({ description: {'$regex': searchDescription, '$options': 'i' } })
            .skip((size * page) - size)
            .limit(size)
            .then( genre => {
                if(genre.length === 0){
                    return res.status(200).send({
                        code: 200,
                        message: "There is no genre registered.",
                        query: "description=" + searchDescription
                    })
                }
                numberOfRecords = genre.length;
                return res.status(200).send({genre, page, size, numberOfRecords});
            })
            .catch(err => {
                return res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the list of genres.",
                    server_message: err.message
                })
            })
    },

    // Resource to be called through the method POST and the path /genres
    async store(req, res) {
        const { id, description } = req.body;

        await Genre.findOne({ description })
            .then( genre => {
                if(genre){
                    return res.status(400).send({
                        code: 400,
                        message: "The genre " + description + " already exists."
                    })
                }
                Genre.create({ id, description })
                    .then( genre => {
                        return res.status(200).send({
                            code: 200,
                            message: "Genre successfully created.",
                            genre
                        })
                    })
                    .catch(err => {
                        return res.status(500).send({
                            code: 500,
                            message: "Error in creating the genre " + genre + ".",
                            server_message: err.message
                        })
                })
            })
            .catch(err => {
                return res.status(500).send({
                    code: 500,
                    message: "Error in checking if the genre " + description + " exist.",
                    server_message: err.message
                })
            });
    },

    // Resource to be called through the method GET and the path /genres/:genreId
    async show(req,res){
        const { genreId } = req.params;

        await Genre.findOne({ _id: genreId })
            .then(genre => {
                if(!genre){
                    return res.status(404).send({
                        code: 404,
                        message: "There is no genre registered. Genre id is " + genreId
                    })
                }
                return res.status(200).json(genre);
            }).catch(err => {
                if(err.kind === "ObjectId"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "ObjectId is not formatted correctly. ObjectId is " + genreId,
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the genre. Genre id: " + genreId,
                    server_message: err
                })
            })
    },

    // Resource to be called through the method PUT and the path /genres/:genreId
    async update(req,res){
        const { genreId } = req.params;

        await Genre.findOneAndUpdate({_id: genreId}, {
            description: req.body.description
        }, {new: true, runValidators: true})
            .then( genre => {
                if(!genre){
                    return res.status(200).send({
                        code: 200,
                        message: "Genre not found. Genre id: " + genreId,
                    });
                }
                return res.status(200).send(genre);
            }). catch(err => {
                if(err.name === "ValidationError"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "Data validation error.",
                        server_error_name: err.name,
                        server_error_message: err.message,
                    })
                }
                if(err.kind === "ObjectId"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "ObjectId is not formatted correctly. ObjectId is " + genreId,
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in updating genre with id " + genreId,
                    server_message: err
                });
            });
    }
};