const Artist = require('../models/Artist');
const Movie = require('../models/Movie');

module.exports = {

    // Resource to be called through the method GET and the path /artists
    async index (req,res){

        let { page } = req.query;
        let { size } = req.query;

        if(!page || (page < 1)){
            page = 1;
        }else{
            page = parseInt(page);
        }

        if(!size || (size < 10)){
            size = 10;
        }else{
            size = parseInt(size);
        }

        let searchFirstName = req.query.firstName;
        let searchLastName = req.query.lastName;

        if(!searchFirstName){
            searchFirstName = ""
        }
        if(!searchLastName){
            searchLastName = ""
        }

        let numberOfRecords = 0;
        await Artist.find({
            firstName: {'$regex': searchFirstName, '$options': 'i' },
            lastName: {'$regex': searchLastName, '$options': 'i' }})
            .skip((size * page) - size)
            .limit(size)
            .then(artist => {
                if(artist.length === 0){
                    return res.status(200).send({
                        code: 200,
                        message: "There is no artist registered.",
                        query: "firstname=" + searchFirstName + "&lastname=" + searchLastName
                    })
                }
                numberOfRecords = artist.length;
                return res.status(200).send({artist, page, size, numberOfRecords})
            })
            .catch(err => {
                return res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the list of artists.",
                    server_message: err
                })
            })
    },

    // Resource to be called through the method POST and the path /artists
    async store(req, res) {
        const { id, firstName, lastName, dateOfBirth } = req.body;

        await Artist.create({ id, firstName, lastName, dateOfBirth })
            .then(artist => {
                return res.status(200).send({
                    code: 200,
                    message: "Artist successfully created.",
                    artist
                })
            })
            .catch(err => {
                if(err.name === "ValidationError"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "Data validation error.",
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in creating the artist.",
                    server_message: err
                })
            })
    },

    // Resource to be called through the method GET and the path /artists/:artistId
    async show(req, res){
        const { artistId } = req.params;
        await Artist.findOne({_id: artistId})
            .then(artist => {
                if(!artist){
                    return res.status(404).send({
                        code: 404,
                        message: "There is no artist registered. Artist id is " + artistId
                    })
                }
                return res.status(200).send(artist)
            })
            .catch(err => {
                if(err.kind === "ObjectId"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "ObjectId is not formatted correctly. ObjectId is " + artistId,
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the artist. Artist id: " + artistId,
                    server_message: err
                })
            })

    },

    // Resource to be called through the method PUT and the path /artists/:artistId
    async update(req, res){
        const { artistId } = req.params;

        await Artist.findByIdAndUpdate({_id: artistId}, {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            dateOfBirth: req.body.dateOfBirth
        }, {new: true, runValidators: true})
            .then(artist => {
                if(!artist){
                    return res.status(200).send({
                        code: 200,
                        message: "Artist not found. Artist id: " + artistId
                    });
                }
                return res.status(200).send(artist);
            })
            .catch(err => {
                if(err.name === "ValidationError"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "Data validation error.",
                        server_error_name: err.name,
                        server_error_message: err.message,
                    })
                }
                if(err.kind === "ObjectId"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "ObjectId is not formatted correctly. ObjectId is " + artistId,
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in updating artist with id " + artistId,
                    server_message: err
                })
            });
    },

    // Resource to be called through the method GET and the path /artists/:artistId/filmography
    async filmography(req, res) {
        const { artistId } = req.params;
        let { page } = req.query;
        let { size } = req.query;

        if(!page || (page < 1)){
            page = 1;
        }else{
            page = parseInt(page);
        }

        if(!size || (size < 10)){
            size = 10;
        }else{
            size = parseInt(size);
        }

        let numberOfRecords = 0;

        await Artist.findOne({ _id: artistId })
            .skip((size * page) - size)
            .limit(size)
            .then(artist => {
                if(!artist){
                    return res.status(400).send({
                        code: 400,
                        message: "Artist not found. Artist id: " + artistId
                    })
                }
                Movie.find( {cast: artistId})
                    .populate('director')
                    .populate('cast')
                    .populate('genres')
                    .then( movie => {
                        if(movie.length === 0){
                            return res.status(200).send({
                                code: 200,
                                message: "Artist with no filmography."
                            })
                        }
                        numberOfRecords = movie.length;
                        return res.status(200).send({movie, page, size, numberOfRecords})
                    })
                    .catch(err => {
                        return res.status(500).send({
                            code: 500,
                            message: "Error in finding the filmography.",
                            server_message: err
                        })
                    });
            })
            .catch(err => {
                return res.status(500).send({
                    code: 500,
                    message: "Error in finding the artist.",
                    server_message: err
                })
            });
    }
};