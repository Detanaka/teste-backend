const Movie = require('../models/Movie');
const Artist = require('../models/Artist');
const Genre = require('../models/Genre');

module.exports = {

    // Resource to be called through the method GET and the path /movies
    async index (req,res) {

        let { page } = req.query;
        let { size } = req.query;

        if(!page || (page < 1)){
            page = 1;
        }else{
            page = parseInt(page);
        }

        if(!size || (size < 10)){
            size = 10;
        }else{
            size = parseInt(size);
        }

        let searchTitle = req.query.title;
        if(!searchTitle){
            searchTitle = ""
        }

        let numberOfRecords = 0;

        await Movie.find({ title: {'$regex': searchTitle, '$options': 'i' } })
            .populate('director')
            .populate('cast')
            .populate('genres')
            .skip((size * page) - size)
            .limit(size)
            .then(movie => {
                if(movie.length === 0){
                    return res.status(200).send({
                        code: 200,
                        message: "There is no movie registered."
                    })
                }
                numberOfRecords = movie.length;
                return res.status(200).send({movie, page, size, numberOfRecords})
            })
            .catch(err => {
                return res.status(500).send({
                    message: "Error in retrieving the list of artists.",
                    server_message: err
                })
            })

    },

    // Resource to be called through the method GET and the path /movies/:movieId
    async show(req, res) {

        const { movieId } = req.params;

        await Movie.findOne({ _id: movieId })
            .populate('director')
            .populate('cast')
            .populate('genres')
            .then(movie => {
                if(!movie){
                    return res.status(404).send({
                        code: 404,
                        message: "There is no movie registered. Movie id is " + movieId
                    })
                }
                return res.status(200).send(movie)
            })
            .catch(err => {
                if(err.kind === "ObjectId"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "ObjectId is not formatted correctly. ObjectId is " + movieId,
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the movie. Movie id: " + movieId,
                    server_message: err
                })
            })
    },

    // Resource to be called through the method PUT and the path /movies/:movieId
    async update(req, res) {

        const { movieId } = req.params;
        const { title, releaseYear, genres, director, cast } = req.body;

        // Find the director
        const directorFound = await Artist.findOne({ _id: director })
            .then(director => {
                if(!director){
                    return res.status(400).send({
                        code: 400,
                        message: "There is no director registered. Director id is " + director._id
                    })
                }
                return director
            })
            .catch(err => {
                res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the director. Director id is " + director,
                    server_message: err
                })
            });

        // Find the genre ids.
        if(genres.length === 0){
            return res.status(400).send({
                code: 400,
                message: "Genre is required. Enter at least one genre."
            })
        }

        let genresList = [];
        for(let i = 0; i < genres.length; i++){
            genresList.push(
                await Genre.findOne({ _id: genres[i]})
                    .then(genre => {
                        if(!genre){
                            return res.status(400).send({
                                code: 400,
                                message: "Genre not found. Genre id is " + genres[i]
                            })
                        }
                        return genre;
                    })
                    .catch(err => {
                        return res.status(500).send({
                            code: 500,
                            message: "Error in retrieving the genre " + genres[i],
                            server_message: err
                        })
                    })
            );
        }

        // Find cast ids
        if(cast.length === 0){
            return res.status(400).send({
                code: 400,
                message: "Cast is required. Enter at least one artist."
            })
        }

        let castIds = [];
        for(let i = 0; i < cast.length; i++){
            castIds.push(
                await Artist.findOne({ _id: cast[i] })
                    .then(artist => {
                        if(!artist){
                            return res.status(400).send({
                                code: 400,
                                message: "Artist not found. Artist id is " + artist[i]
                            })
                        }
                        return artist
                    })
                    .catch(err => {
                        return res.status(500).send({
                            code: 500,
                            message: "Artist not found. Artist id is " + artist[i],
                            server_message: err
                        })
                    })
            );
        }

        await Movie.findByIdAndUpdate({_id: movieId},{
            title,
            releaseYear,
            genres: genresList.map(genre => genre._id),
            director: directorFound._id,
            cast: castIds.map(artist => artist._id)
        }, {new:true, runValidators: true})
            .then(movie => {
                if(!movie){
                    return res.status(400).send({
                        code: 400,
                        message: "Movie not found. Movie id is " + movieId
                    })
                }
                return res.status(200).send(movie)
            })
            .catch(err => {
                if(err.name === "ValidationError"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "Data validation error.",
                        server_error_name: err.name,
                        server_error_message: err.message,
                    })
                }
                if(err.kind === "ObjectId"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "ObjectId is not formatted correctly. ObjectId is " + movieId,
                        server_error_name: err.name,
                        server_error_message: err.message
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in updating movie with id " + movieId,
                    server_message: err
                })
            });
    },

    async store(req, res) {

        const { id, title, releaseYear, genres, director, cast } = req.body;

        // Find the director
        const directorFound = await Artist.findOne({ _id: director })
            .then(director => {
                if(!director){
                    return res.status(400).send({
                        code: 400,
                        message: "There is no director registered. Director id is " + director._id
                    })
                }
                return director
            })
            .catch(err => {
                res.status(500).send({
                    code: 500,
                    message: "Error in retrieving the director. Director id is " + director,
                    server_message: err
                })
            });

        // Find the genre ids.
        if(genres.length === 0){
            return res.status(400).send({
                code: 400,
                message: "Genre is required. Enter at least one genre."
            })
        }

        let genresList = [];
        for(let i = 0; i < genres.length; i++){
            genresList.push(
                await Genre.findOne({ _id: genres[i]})
                    .then(genre => {
                        if(!genre){
                            return res.status(400).send({
                                code: 400,
                                message: "Genre not found. Genre id is " + genres[i]
                            })
                        }
                        return genre;
                    })
                    .catch(err => {
                        return res.status(500).send({
                            code: 500,
                            message: "Error in retrieving the genre " + genres[i],
                            server_message: err
                        })
                    })
            );
        }

        // Find cast ids
        if(cast.length === 0){
            return res.status(400).send({
                code: 400,
                message: "Cast is required. Enter at least one artist."
            })
        }

        let castIds = [];
        for(let i = 0; i < cast.length; i++){
            castIds.push(
                await Artist.findOne({ _id: cast[i] })
                    .then(artist => {
                        if(!artist){
                            return res.status(400).send({
                                code: 400,
                                message: "Artist not found. Artist id is " + artist[i]
                            })
                        }
                        return artist
                    })
                    .catch(err => {
                        return res.status(500).send({
                            code: 500,
                            message: "Artist not found. Artist id is " + artist[i],
                            server_message: err
                        })
                    })
            );
        }

        await Movie.create({
            id,
            title,
            releaseYear,
            genres: genresList.map(genre => genre._id),
            director: directorFound._id,
            cast: castIds.map(artist => artist._id)
        })
            .then(movie => {
                return res.status(200).send({
                    code: 200,
                    message: "Movie successfully created.",
                    movie
                })
            })
            .catch(err => {
                if(err.name === "ValidationError"){
                    return res.status(400).send({
                        code: 400,
                        error_message: "Data validation error.",
                        server_error_name: err.name,
                        server_error_message: err.message,
                    })
                }
                return res.status(500).send({
                    code: 500,
                    message: "Error in creating movie",
                    server_message: err
                })
            })
    },

    async destroy(req, res) {
        const { movieId } = req.params;

        Movie.findByIdAndDelete(movieId)
            .then(movie => {
                if(!movie){
                    return res.status(400).send({
                        code: 400,
                        message: "No movie found with id " + movieId
                    })
                }
                return res.send({ message: "Movie successfully removed."});
            }).catch(err => {
            return res.status(500).send({
                code: 500,
                message: "Error in removing the movie with id " + movieId,
                server_message: err
            })
        });
    }
};