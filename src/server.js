const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');

const app = express();
const port = 3333;

mongoose.connect('mongodb+srv://sciensa:trainees2019@sciensabackend-txr2s.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(express.json());
app.use(routes);

app.listen(port, () =>
    console.log('API escutando na porta ' + port)
);

module.exports = app;