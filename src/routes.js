//index, show, store, update, destroy

const express = require('express');
const GenreController = require('./app/controllers/GenreController');
const ArtistController = require('./app/controllers/ArtistController');
const MovieController = require('./app/controllers/MovieController');

const routes = express.Router();

//Genre routes
routes.post('/genres', GenreController.store);
routes.get('/genres', GenreController.index);
routes.get('/genres/:genreId', GenreController.show);
routes.put('/genres/:genreId', GenreController.update);

//Artist routes
routes.post('/artists', ArtistController.store);
routes.get('/artists', ArtistController.index);
routes.get('/artists/:artistId', ArtistController.show);
routes.put('/artists/:artistId', ArtistController.update);
routes.get('/artists/:artistId/filmography', ArtistController.filmography);

//Movies routes
routes.post('/movies', MovieController.store);
routes.get('/movies', MovieController.index);
routes.get('/movies/:movieId', MovieController.show);
routes.put('/movies/:movieId', MovieController.update);
routes.delete('/movies/:movieId', MovieController.destroy);

module.exports = routes;

