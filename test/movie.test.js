const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('Movie', () => {
    describe('GET: Index', () => {
        it('It should return 200 | check listing of all movies', (done) => {
            chai.request(server)
                .get('/movies')
                .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.movie.should.be.an('array');
                        res.body.movie[0].should.have.property('_id');
                        res.body.movie[0].should.have.property('title');
                        res.body.movie[0].should.have.property('releaseYear');
                        res.body.movie[0].should.have.property('createdAt');
                        res.body.movie[0].should.have.property('updatedAt');
                        done();
                });
        });
    });

    describe('GET: Show', () => {
        it('It should return 200 | check an existing movie', (done) => {
            chai.request(server)
                .get('/movies/5d96475cf08d063b50274fd4')
                .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.have.property('_id');
                        res.body.should.have.property('title');
                        res.body.should.have.property('releaseYear');
                        res.body.should.have.property('createdAt');
                        res.body.should.have.property('updatedAt');
                        res.body.title.should.equal("Matrix");
                        res.body.releaseYear.should.equal(1999);
                        done();
                });
        });
        it('It should return 400 | check an id wrong formated', (done) => {
            chai.request(server)
                .get('/movies/5d96475cf08d063b50274fd')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.code.should.equal(400);
                    res.body.error_message.should.equal('ObjectId is not formatted correctly. ObjectId is 5d96475cf08d063b50274fd');
                    done();
                });
        });
        it('It should return 404 | check an inexisting movie', (done) => {
            chai.request(server)
                .get('/movies/5d96475cf08d063b50271234')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.code.should.equal(404);
                    res.body.message.should.equal('There is no movie registered. Movie id is 5d96475cf08d063b50271234');
                    done();
                })
        });
    });
});