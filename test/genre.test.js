const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('Genres', () => {
    describe('GET: Index', () => {
        it('It should return 200 | check listing of all genres', (done) => {
            chai.request(server)
                .get('/genres')
                .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.genre.should.be.an('array');
                        res.body.genre[0].should.have.property('_id');
                        res.body.genre[0].should.have.property('description');
                        res.body.genre[0].should.have.property('createdAt');
                        res.body.genre[0].should.have.property('updatedAt');
                        done();
                });
        });
    });

    describe('GET: Show', () => {
        it('It should return 200 | check an existing genre', (done) => {
            chai.request(server)
                .get('/genres/5d979c3c1b8bc21ebc9841ff')
                .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.have.property('_id');
                        res.body.should.have.property('description');
                        res.body.should.have.property('createdAt');
                        res.body.should.have.property('updatedAt');
                        res.body.description.should.equal("teste");
                        done();
                });
        });
        it('It should return 400 | check an id wrong formated', (done) => {
            chai.request(server)
                .get('/genres/5d979c3c1b8bc21ebc9841f')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.code.should.equal(400);
                    res.body.error_message.should.equal('ObjectId is not formatted correctly. ObjectId is 5d979c3c1b8bc21ebc9841f');
                    done();
                });
        });
        it('It should return 404 | check an inexisting genre', (done) => {
            chai.request(server)
                .get('/genres/5d979c3c1b8bc21ebc91234f')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.code.should.equal(404);
                    res.body.message.should.equal('There is no genre registered. Genre id is 5d979c3c1b8bc21ebc91234f');
                    done();
                })
        });
    });
});