const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('Artists', () => {
    describe('GET: Index', () => {
        it('It should return 200 | check listing of all artists', (done) => {
            chai.request(server)
                .get('/artists')
                .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.artist.should.be.an('array');
                        res.body.artist[0].should.have.property('_id');
                        res.body.artist[0].should.have.property('firstName');
                        res.body.artist[0].should.have.property('lastName');
                        res.body.artist[0].should.have.property('dateOfBirth');
                        res.body.artist[0].should.have.property('createdAt');
                        res.body.artist[0].should.have.property('updatedAt');
                        done();
                });
        });
    });

    describe('GET: Show', () => {
        it('It should return 200 | check an existing artist', (done) => {
            chai.request(server)
                .get('/artists/5d967b425aa77312b4346865')
                .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.have.property('_id');
                        res.body.should.have.property('firstName');
                        res.body.should.have.property('lastName');
                        res.body.should.have.property('dateOfBirth');
                        res.body.should.have.property('createdAt');
                        res.body.should.have.property('updatedAt');
                        res.body.lastName.should.equal("Leguizamo");
                        res.body.firstName.should.equal("John");
                        res.body.dateOfBirth.should.equal("1964-07-21T14:00:00.000Z");
                        done();
                });
        });
        it('It should return 400 | check an id wrong formated', (done) => {
            chai.request(server)
                .get('/genres/5d967b425aa77312b434686')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.code.should.equal(400);
                    res.body.error_message.should.equal('ObjectId is not formatted correctly. ObjectId is 5d967b425aa77312b434686');
                    done();
                });
        });
        it('It should return 404 | check an inexisting artist', (done) => {
            chai.request(server)
                .get('/artists/5d967b425aa77312b4341234')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.code.should.equal(404);
                    res.body.message.should.equal('There is no artist registered. Artist id is 5d967b425aa77312b4341234');
                    done();
                })
        });
    });
});